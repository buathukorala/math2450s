@def title = "WebWork"

# Homework

![Alt Text](/assets/HW.jpg)


## WebWork assignments

* [HW 14](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Tuesday, May 3rd. 
* [HW 13](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Friday, April 30th. 
* [HW 12](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Tuesday, April 20th. 
* [HW 11](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Monday, April 12th.  
* [HW 10](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Tuesday, April 6th.  
* [HW 9](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Tuesday, March 30th.  
* [HW 8](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Monday, March 22nd.  
* [HW 7](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Wednesday, March 17th.  
* [HW 6](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Monday, March 8th.  
* [HW 5](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Monday, February 22nd.  
* [HW 4](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Thursday, February 18th.  
* [HW 3](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Wednesday, February 10th.  
* [HW 2](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Monday, February 2nd. 
* [HW 1](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) is posted. Due on Wednesday, January 27th. 




## Useful WeBWork Info

* Head over to the [WebWork](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) 
* [Click here](https://drive.google.com/file/d/1dbAEspqW6VyGxjR9nSszjoPdFLXmZdbq/view?usp=sharing) to see a tutorial on how to use WeBWork
* [Click here](https://drive.google.com/file/d/1bzmKCZdGB2-xVwkSrNj16bxw4qKGWTin/view?usp=sharing) to read the tutorial on entering answers in WeBWork





