@def title = "Class Notes"

# Class Notes

![Alt Text](/assets/notes3.jpg)

## Week 16

* Wednesday, May 5th 
    - [Review 4 (mostly from chapter 13)](https://tinyurl.com/texvxwff)
    - Missed the review, No worries [click here](https://youtu.be/zq5xrWM1CtA)

* Monday, May 3rd
    - [Lecture note: Chap 13.5 (part2)](https://tinyurl.com/4jztjn9r)
    - Missed the lecture, No worries [click here](https://youtu.be/a77wq1jpYi8)

## Week 15

* friday, Apr. 30th 
    - [Lecture note: Ch 13.5 (part1)](https://tinyurl.com/4jztjn9r)
    - [Lecture note: Ch 13.4 (part2)](https://tinyurl.com/8rh3khhx)
    - Missed the lecture, No worries [click here](https://youtu.be/4rvPiTbCM2U)

* Wednesday, Apr. 28th 
    - [Lecture note: Ch 13.4 (part1)](https://tinyurl.com/ah4wcyxj)
    - [Lecture note: Ch 13.3 (part3)](https://tinyurl.com/24hzdpbr)
    - Missed the lecture, No worries [click here](https://youtu.be/NyvxkmLlOPg)

* Monday, Apr. 26th
    - [Lecture note: Chap 13.3 (part2)](https://tinyurl.com/3pb8rup3)
    - Missed the lecture, No worries [click here](https://youtu.be/dkY00GIwUrE)

## Week 14

* Friday, Apr. 23rd
    - Missed the lecture, No worries [click here](https://youtu.be/8yc5QGkoePc)

* Wednesday, Apr. 21st 
    - [Lecture note: Ch 13.2 (part1)](https://tinyurl.com/xukw7zjb)
    - Missed the lecture, No worries [click here](https://youtu.be/9SSUWyM_Xdo)

* Monday, Apr. 19th
    - [Lecture note: Chap 13.1](https://tinyurl.com/2fhkhfnr)
    - Missed the lecture, No worries [click here](https://youtu.be/H8r9cCbX4ak)


## Week 13

* Friday, Apr. 16th
    - [Lecture note: Ch 12.7 (part2)](https://tinyurl.com/3x8tusbn) 
    - Missed the lecture, No worries [click here](https://youtu.be/CpNAZwvGwug)

* Wednesday, Apr. 14th
    - [Lecture note: Ch 12.7 (part1)](https://tinyurl.com/kfcyzvy8)
    - [Lecture note: Ch 12.5 (part3)](https://tinyurl.com/kwywwbam)
    - Missed the lecture, No worries [click here](https://youtu.be/ofFA71h1Rjw)

* Monday, Apr. 12th
    - [Lecture note: Chap 12.5 (part2)](https://tinyurl.com/kwywwbam)
    - Missed the lecture, No worries [click here](https://youtu.be/FeEdQIz0wp0)


## Week 12

* Friday, Apr. 9th
    - [Lecture note: Ch 12.5 (part1)](https://tinyurl.com/h54f7s) 
    - [Lecture note: Chap 12.4](https://tinyurl.com/k5dexdfn)
    - Missed the lecture, No worries [click here](https://youtu.be/CuBGZVp4cgI)

* Wednesday, Apr. 7th
    - [Lecture note: Ch 12.3 (part2)](https://tinyurl.com/ypxdspc8)
    - Missed the lecture, No worries [click here](https://youtu.be/YdOa7jpJU7c)



## Week 11

* Friday, Apr. 2nd
    - [Lecture note: Ch 12.3 (part1)](https://tinyurl.com/2cfedyp7) 
    - [Lecture note: Chap 12.2 (part2)](https://tinyurl.com/ynyasc5d)
    - Missed the lecture, No worries [click here](https://youtu.be/gjwes6c-V1o)

* Wednesday, Mar. 31st
    - [Lecture note: Ch 12.2 (part1)](https://tinyurl.com/ynyasc5d)
    - [Lecture note: Ch 12.1 (part2)](https://tinyurl.com/hj636unn)
    - Missed the lecture, No worries [click here](https://youtu.be/YHY0-SuO8bg)

* Monday, Mar. 29th
    - [Lecture note: Chap 12.1 (part1)](https://tinyurl.com/vsr5e9jm)
    - Missed the lecture, No worries [click here](https://youtu.be/XocjSie7FHk)

## Week 10

* Friday, Mar. 26th
    - [Lecture note: Chap 11.7 (part2)](https://tinyurl.com/ez78yv7x)
    - Missed the lecture, No worries [click here](https://youtu.be/XYwUbK079jw)


* Monday, Mar. 22nd
    
    - [Exam 2 review with answers](https://tinyurl.com/36zx6kn5)
    - Missed the review, No worries [click here](https://youtu.be/sAAEpE14iWw)

    - [Lecture note: Chap 11.7 (part1)](https://tinyurl.com/5zw38jsw)
    - Missed the lecture, No worries [click here](https://youtu.be/nvVMqo3WZ8w)

## Week 9

* Wednesday, Mar. 17th
    
    - [Lecture note: Chap 11.6 (complete)](https://tinyurl.com/jvf98bvu)
    - Missed the lecture, No worries [click here](https://youtu.be/Pyuuex7wdjc)


* Monday, Mar. 15th

    - [Lecture note: Chap 11.6 (part2)](https://tinyurl.com/32n254f3)
    - Missed the lecture, No worries [click here](https://youtu.be/C6moc6U_r30)


## Week 8 

* Friday, Mar. 12th

    - [Lecture note: Chap 11.6 (part1)](https://tinyurl.com/56fhb44b)
    - Missed the lecture, No worries [click here](https://youtu.be/3ZdjEp17qck)

* Wednesday, Mar. 10th

    - [Lecture note: Chap 11.4 (complete)](https://tinyurl.com/yazf4t72)
    - Missed the lecture, No worries [click here](https://youtu.be/1H40mLYuufE)


* Monday, Mar. 8th
    - [Lecture note: Chap 11.4 (part1)](https://tinyurl.com/h4k88ffe)
    - [Lecture note: Chap 11.5 (complete)](https://tinyurl.com/3rwpzc6d)
    - Missed the lecture, No worries [click here](https://youtu.be/e9CYs1vpKMg)


## Week 7 

* Friday, Mar. 5th
    - [Lecture note: Chap 11.5 (part1)](https://tinyurl.com/64rps7sy)
    - [Lecture note: Chap 11.3 (complete)](https://tinyurl.com/3a5hdxee)
    - Missed the lecture, No worries [click here](https://youtu.be/gjb-LpBsSvo)


* Wednesday, Mar. 3rd

    - [Lecture note: Chap 11.3 (part2)](https://tinyurl.com/4wjp6jsh)
    - Missed the lecture, No worries [click here](https://youtu.be/DdnJKWL77qc)

* Monday, Mar. 1st
    
    - [Lecture note: Chap 11.3 (part 1)](https://tinyurl.com/4wjp6jsh)
    - Missed the lecture, No worries [click here](https://youtu.be/lwtgWo6v1Lg)



## Week 6 

* Exam 1: Review session 

    - [Review problems with answers](https://tinyurl.com/1hibpadr)
    - Missed the review session, No worries [click here](https://youtu.be/RLEYRZrxXkQ)

* Monday, Feb. 22nd
    
    - [Lecture note: Chap 11.2 (complete)](https://tinyurl.com/36bctv4p)
    - Missed the lecture, No worries [click here](https://youtu.be/_zEjlnYqxes)



## Week 5 

* Friday, Feb. 19th

    - [Lecture note: Chap 11.2 (part2)](https://tinyurl.com/5gopncdy)
    - Missed the lecture, No worries [click here](https://youtu.be/UST-G202za0)

* Wednesday, Feb. 17th

    - [Lecture note: Chap 11.2 (part1)](https://tinyurl.com/ygap7j25)
    - [Lecture note: Chap 11.1 (part2)](https://tinyurl.com/l8n3zugj)
    - Missed the lecture, No worries [click here](https://youtu.be/FBB_CZBa-K4)


* Monday, Feb. 15th

    - [Lecture note: Chap 11.1 (part1)](https://tinyurl.com/52rt37kp)
    - Missed the lecture, No worries [click here](https://youtu.be/gbp7RBO2VTo)


## Week 4 

* Friday, Feb. 12th

    - [Lecture note: Chap 10.4 (complete)](https://tinyurl.com/1s4nonsl)
    - Missed the lecture, No worries [click here](https://youtu.be/rlTVzyLY2o0)


* Wednesday, Feb. 10th

    - [Lecture note: Chap 10.4 (part2)](https://tinyurl.com/1s4nonsl)
    - Missed the lecture, No worries [click here](https://youtu.be/VnOSPrg1SEQ)


* Monday, Feb. 8th

    - [Lecture note: Chap 10.4 (part1)](https://tinyurl.com/33ngr7a5)
    - Missed the lecture, No worries [click here](https://youtu.be/7pWaL75q5Sg)
    

## Week 3 

* Friday, Feb. 5th

    - [Lecture note: Chap 10.2](https://tinyurl.com/3neo324x)
    - Missed the lecture, No worries [click here](https://youtu.be/VFyJ0s8kfQY)

* Wednesday, Feb. 3rd

    - [Lecture note: Chap 10.1](https://tinyurl.com/ytuvobx8)
    - Missed the lecture, No worries [click here](https://youtu.be/_xanaStRmIE)

* Monday, Feb. 1st

    - [Lecture note: Chap 9.7](https://tinyurl.com/krarrbx5)
    - Missed the lecture, No worries [click here](https://youtu.be/1CS8jaAM3Oc)


## Week 2 

* Friday, Jan. 29th

    - [Lecture note: Chap 9.6](https://tinyurl.com/y2cvh5ej)
    - Missed the lecture, No worries [click here](https://youtu.be/SvvTn97mavs)

* Wednesday, Jan. 27th

    - [Lecture note: Chap 9.5 (part 2)](https://tinyurl.com/y4nxctma)
    - Missed the lecture, No worries (sorry, missed the first half of the lecture) [click here](https://youtu.be/r-k8H8jNDyY)

* Monday, Jan. 25th

    - [Lecture note: Chap 9.5 (part 1)](https://tinyurl.com/yyjkkuph)
    - [Lecture note: Chap 9.1-9.4 (complete)](https://tinyurl.com/y4jsav44)
    - Missed the lecture, No worries [click here](https://youtu.be/ScrwdsPd2zE)

## Week 1 

* Friday, Jan. 22nd
    - [Lecture note: Chap 9.1-9.4 (review - part 1)](https://www.amazon.com/clouddrive/share/vxAObBie3mny60P3VY9eENxP20bJbKkMkBLSENeQyke)
    - Missed the lecture, No worries [click here](https://youtu.be/vPaeGwpXIOg)

* Wednesday, Jan. 20th
    - [Welcome slides!](https://drive.google.com/file/d/1aXi_cCHYkxbXudmQIXt7ehwsdxQIylO_/view?usp=sharing)



