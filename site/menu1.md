@def title = "Announcements"

# Announcements

![Alt Text](/assets/news.jpg)

## Week 15 

* HW 14 is posted in WeBWork.  closes Tuesday, 05/04/2021, at 11:59pm CDT.


## Week 12 

* Quiz 6 is posted in WebWork. Due on Friday, April 9th.
* HW 11 is posted in WeBWork.  closes Monday, 04/12/2021, at 11:59pm CDT.


## Week 8 

* Quiz 5 is posted in WebWork. Due on Friday, March 12th.
* HW 7 is posted in WeBWork.  Due on Wednesday, March 17th. 


## Week 7 

* HW 6 is posted in WeBWork.  Due on Monday, March 8th. 



## Week 6

* Exam 1: February 24th at 11:00AM CDT.

* Install [Proctorio Chrome Extension](https://getproctorio.com/) 
     @@colbox-blue
     [Proctorio Quick Start Guide](https://drive.google.com/file/d/1L3AUF83Pq3Wz2aVIw6mVoTYKpWG1uEc7/view?usp=sharing)
     @@

     
## Week 5 
* HW 5 is posted in WeBWork.  Due on Monday, February 22nd. 

## Week 4 

* Quiz 4 is posted in WebWork. Due on Friday, Feruary 12th.
* HW 4 is posted in WeBWork.  Due on Tuesday, February 16th. 


## Week 3 

* Quiz 3 is posted in WebWork. Due on Friday, Feruary 5th.
* HW 3 is posted in WeBWork.  Due on Wednesday, Feruary 10th.


## Week 2 

* Quiz 2 is posted in WebWork. Due on Friday, January 29th.
* HW 2 is posted in WeBWork.  Due on Monday, Feruary 2nd.


## Week 1 

* Install [Proctorio Chrome Extension](https://getproctorio.com/) 
     @@colbox-blue
     [Proctorio Quick Start Guide](https://drive.google.com/file/d/1L3AUF83Pq3Wz2aVIw6mVoTYKpWG1uEc7/view?usp=sharing)
     @@
* [Quiz 1 lives here!](https://forms.gle/SYoj4hN1gBhhWmzeA) Due on Friday, January 22nd
* HW 1 is posted in WeBWork: Due on Wednesday, January 27th. 




