@def title = "Welcome"
@def tags = ["syntax", "code"]

# Welcome to the World of Multivariable Calculus! 

 
<!-- \tableofcontents  you can use \toc as well -->

**_MATH 2450: Most important math class you will ever take!_**

![Alt Text](/assets/toronto.gif)



---

[Anonymous Lecture Feedback](https://forms.gle/8WLYSjbFQnzFSoSM8)

---

* Head over to the [WebWork](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/)
* Head over to the [Blackboard](https://ttu.blackboard.com/)

---

## Things to be known:

* Final Exam (cumulative): Monday, May 10th, 1:30 p.m. to 4:00 p.m. (CDT) via Blackboard + Proctorio

* Final Exam info [lives here!](https://tinyurl.com/846msuy2)

----

Course syllabus [lives here!](https://tinyurl.com/2vc8c7ej)


* **Lecture:** MW: 11:00-11:50 & F: 10:00-11:50, MCOM 053 
    - Live class session via ZOOM: [Click here!!!](https://zoom.us/j/8065430626)  
* **Instructor:** Bhagya Athukorallage, PhD
* **Office Hours through Zoom:**  MW: 10:00am - 10:45am & MWF: 2:00pm - 3:00pm or by appointments
    - [Click here to enter my ZOOM office](https://zoom.us/j/8065430626)
* **Office:** MATH 18C
* **E-mail:** bhagya.athukorala@ttu.edu







